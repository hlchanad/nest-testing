import { registerAs } from '@nestjs/config';
import { LoggerModuleOptions } from '@base';

export default registerAs(
  'logger',
  (): LoggerModuleOptions => ({
    isEnabled: true,
    logLevel: 'debug',
  })
);
