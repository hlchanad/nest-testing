import { registerAs } from '@nestjs/config';

export default registerAs('app', () => ({
  name: process.env.APP_NAME || 'nestjs',
  environment: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 4000,
}));
