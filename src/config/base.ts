import { registerAs } from '@nestjs/config';
import { BaseModuleOptions } from '@base';

export default registerAs(
  'base',
  (): Omit<
    BaseModuleOptions,
    'loggerModuleOptions' | 'migrationModuleOptions' | 'commandModuleOptions'
  > => ({
    requestIdFn: () => Math.floor(Math.random() * 900000 + 100000).toString(),

    loggerMiddlewareOptions: {
      excludeHeaders: ['cookie'],
      excludeRoutes: [{ method: 'GET', endpoint: '/favicon.ico' }],
    },
  })
);
