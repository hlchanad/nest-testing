import { registerAs } from '@nestjs/config';

import { NamingStrategies } from '@base';
import { ConnectionOptions } from 'typeorm';

export default registerAs('database', () => {
  const primary: ConnectionOptions = {
    // @ts-ignore
    type: process.env.DATABASE_DRIVER,
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_SCHEMA,
    entities: ['dist/**/*.entity{.ts,.js}'],
    namingStrategy: new NamingStrategies.MysqlSnake(),
    synchronize: false,
    logging: false,
    migrationsRun: false,
    migrations: ['dist/migrations/**/*{.ts,.js}'],
    cli: {
      migrationsDir: 'src/migrations',
    },
  };

  return { primary };
});
