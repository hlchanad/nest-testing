import * as appRootPath from 'app-root-path';
import * as glob from 'glob';

const ensureLeadingSlash = (string) => {
  if (string == null) return '';

  string = string.trim();
  if (string === '') return '';

  return string.startsWith('/') ? string : '/' + string;
};

export function globPaths(pattern: string): string[] {
  return glob.sync(pattern);
}

export function appRoot(path?: string): string {
  return appRootPath.path + ensureLeadingSlash(path);
}

export function sourcePath(path?: string): string {
  return appRoot('/src' + ensureLeadingSlash(path));
}
