import { ConfigFactory } from '@nestjs/config/dist/interfaces/config-factory.interface';

import * as configs from '@src/config';

export function factories(): ConfigFactory[] {
  return Object.values(configs).map((config) => config.default);
}
