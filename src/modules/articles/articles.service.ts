import { Injectable } from '@nestjs/common';

import { BaseService } from '@base';

import { ArticlesRepository } from './articles.repository';

@Injectable()
export class ArticlesService extends BaseService {
  constructor(private articlesRepository: ArticlesRepository) {
    super();
  }
}
