import { Column, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from '@base';
import { ArticleTranslation } from '@src/modules/articles/article-translations.entity';

@Entity()
export class Article extends BaseEntity {
  @Column()
  publishAt: Date;

  @OneToMany(
    (type) => ArticleTranslation,
    (translation) => translation.article,
    { cascade: true }
  )
  translations: ArticleTranslation[];
}
