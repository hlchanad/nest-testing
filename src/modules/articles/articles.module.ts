import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ValidatorService } from '@base';

import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { ArticlesRepository } from './articles.repository';
import { Article } from './articles.entity';
import { ArticleTranslation } from './article-translations.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Article, ArticleTranslation])],
  controllers: [ArticlesController],
  providers: [ArticlesService, ArticlesRepository],
})
export class ArticlesModule implements NestModule {
  constructor(private readonly validatorService: ValidatorService) {}

  configure(consumer: MiddlewareConsumer): any {
    this.validatorService.registerMiddlewares(consumer, [ArticlesController]);
  }
}
