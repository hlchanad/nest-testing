import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BaseRepository } from '@base';

import { Article } from './articles.entity';
import { CreateArticleDto } from '@src/modules/articles/dtos';

@Injectable()
export class ArticlesRepository extends BaseRepository<Article> {
  constructor(
    @InjectRepository(Article) protected repository: Repository<Article>
  ) {
    super();
  }

  findAll(): Promise<Article[]> {
    return this.repository.find({
      where: { deletedAt: null },
      relations: ['translations'],
    });
  }

  findById(id: number): Promise<Article> {
    return this.repository.findOne({
      where: { id, deletedAt: null },
      relations: ['translations'],
    });
  }

  create(createDto: CreateArticleDto): Promise<Article> {
    const article = this.repository.create(createDto);
    return this.save(article);
  }

  update(article: Article): Promise<Article> {
    return this.save(article);
  }

  async remove(id: number): Promise<void>;
  async remove(article: Article): Promise<void>;
  async remove(idOrArticle: number | Article): Promise<void> {
    const article =
      typeof idOrArticle === 'number'
        ? await this.repository.findOne({ where: { id: idOrArticle } })
        : idOrArticle;

    await this.delete(article);
  }
}
