import { IsArray, IsDate, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';

class TranslationDto {
  language: string;
  title: string;
  content: string;
}

export class CreateArticleDto {
  @IsDate()
  publishAt: Date;

  @IsArray()
  translations: TranslationDto[];
}
