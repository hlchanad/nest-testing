import { IsArray, IsDate } from 'class-validator';

class TranslationDto {
  language: string;
  title: string;
  content: string;
}

export class UpdateArticleDto {
  @IsDate()
  publishAt: Date;

  @IsArray()
  translations: TranslationDto[];
}
