import { Column, Entity, ManyToOne } from 'typeorm';

import { BaseTranslationEntity } from '@base';

import { Article } from './articles.entity';

@Entity()
export class ArticleTranslation extends BaseTranslationEntity {
  @Column()
  articleId: number;

  @Column()
  title: string;

  @Column()
  content: string;

  @ManyToOne((type) => Article, (article) => article.translations)
  article: Article;
}
