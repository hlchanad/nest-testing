import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import {
  AppResponse,
  BaseAppResponses,
  RecordNotFoundException,
  Validate,
} from '@base';

import { ArticlesRepository } from './articles.repository';
import { ArticlesService } from './articles.service';
import { CreateArticleDto, UpdateArticleDto } from './dtos';

@Controller('/articles')
export class ArticlesController {
  constructor(
    private articlesRepository: ArticlesRepository,
    private articlesService: ArticlesService
  ) {}

  @Get()
  async getAll(): Promise<AppResponse<any>> {
    const articles = await this.articlesRepository.findAll();
    return BaseAppResponses.OK.clone().setData({ articles });
  }

  @Get('/:id')
  async getById(@Param('id') id: number): Promise<AppResponse<any>> {
    const article = await this.articlesRepository.findById(id);

    if (!article) {
      throw new RecordNotFoundException();
    }

    return BaseAppResponses.OK.clone().setData({ article });
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @Validate.Body({
    publishAt: 'date|required',
    translations: 'array|required',
    'translations.*.language': 'string|required',
    'translations.*.title': 'string|required',
    'translations.*.content': 'string|required',
  })
  async create(@Body() createDto: CreateArticleDto): Promise<AppResponse<any>> {
    const article = await this.articlesRepository.create(createDto);
    return BaseAppResponses.OK.clone().setData({ article });
  }

  @Put('/:id')
  @Validate.Body({
    publishAt: 'date',
    translations: 'array',
    'translations.*.language': 'string|required',
    'translations.*.title': 'string',
    'translations.*.content': 'string',
  })
  async update(
    @Param('id') id: number,
    @Body() updateDto: UpdateArticleDto
  ): Promise<AppResponse<any>> {
    const article = await this.articlesRepository.findById(id);

    if (!article) {
      throw new RecordNotFoundException();
    }

    await this.articlesRepository.update(
      this.articlesService.updateModel(article, updateDto)
    );

    return BaseAppResponses.OK.clone();
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async destroy(@Param('id') id: number): Promise<AppResponse<any>> {
    const article = await this.articlesRepository.findById(id);

    if (!article) {
      throw new RecordNotFoundException();
    }

    await this.articlesRepository.remove(article);

    return BaseAppResponses.OK.clone();
  }
}
