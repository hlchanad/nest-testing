import { HttpStatus } from '@nestjs/common';

import { AppException } from './app.exception';
import { BaseAppResponses } from '../constants';

export class RecordNotFoundException extends AppException {
  constructor() {
    super(HttpStatus.NOT_FOUND, BaseAppResponses.RecordNotFound);
  }
}
