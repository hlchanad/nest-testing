import { HttpStatus } from '@nestjs/common';

import { IAppResponse } from '@base';

export interface SerializeError {
  statusCode(): HttpStatus;
  serializeError(): IAppResponse<any>;
}
