import { HttpStatus } from '@nestjs/common';

import { AppResponse, IAppResponse } from '../models';
import { SerializeError } from './serialize-error';

export class AppException extends Error implements SerializeError {
  constructor(
    private httpStatus: HttpStatus,
    private appResponse: AppResponse<any>
  ) {
    super();
    Object.setPrototypeOf(this, AppException.prototype);
  }

  statusCode(): HttpStatus {
    return this.httpStatus;
  }

  serializeError(): IAppResponse<any> {
    return this.appResponse.toJSON();
  }
}
