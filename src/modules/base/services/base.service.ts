import { DeepPartial } from 'typeorm';

export class BaseService {
  updateModel<T>(
    model: T,
    dto: DeepPartial<T>,
    {
      localeField = 'language',
      translationsField = 'translations',
    }: { localeField?: string; translationsField?: string } = {}
  ): T {
    Object.getOwnPropertyNames(dto)
      .filter((name) => ![translationsField].includes(name))
      .forEach((name) => (model[name] = dto[name]));

    if (
      Array.isArray(dto[translationsField]) &&
      Array.isArray(model[translationsField])
    ) {
      dto[translationsField].forEach((dtoTranslation) => {
        let modelTranslation = model[translationsField].find(
          (modelTranslation) =>
            modelTranslation[localeField] === dtoTranslation[localeField]
        );

        if (!modelTranslation) {
          modelTranslation = { [localeField]: dtoTranslation[localeField] };
          model[translationsField].push(modelTranslation);
        }

        Object.getOwnPropertyNames(dtoTranslation)
          .filter((name) => ![localeField].includes(name))
          .forEach((name) => (modelTranslation[name] = dtoTranslation[name]));
      });
    }

    return model;
  }
}
