import * as _ from 'lodash';
import * as pluralize from 'pluralize';
import { DefaultNamingStrategy, NamingStrategyInterface } from 'typeorm';

// would like to extend typeorm-naming-strategy.SnakeNamingStrategy
// https://www.npmjs.com/package/typeorm-naming-strategies
// but no idea why cannot extend
// then maybe just copy logic to here
export class MysqlSnake extends DefaultNamingStrategy
  implements NamingStrategyInterface {
  constructor() {
    super();
  }

  tableName(targetName: string, userSpecifiedName: string | undefined): string {
    return userSpecifiedName
      ? userSpecifiedName
      : _.snakeCase(pluralize(targetName));
  }

  columnName(
    propertyName: string,
    customName: string,
    embeddedPrefixes: string[]
  ): string {
    return (
      _.snakeCase(embeddedPrefixes.join('_')) +
      (customName ? customName : _.snakeCase(propertyName))
    );
  }

  relationName(propertyName: string): string {
    return _.snakeCase(propertyName);
  }

  joinColumnName(relationName: string, referencedColumnName: string): string {
    return _.snakeCase(relationName + '_' + referencedColumnName);
  }

  joinTableName(
    firstTableName: string,
    secondTableName: string,
    firstPropertyName: string,
    secondPropertyName: string
  ): string {
    return _.snakeCase(
      firstTableName +
        '_' +
        firstPropertyName.replace(/\./gi, '_') +
        '_' +
        secondTableName
    );
  }

  joinTableColumnName(
    tableName: string,
    propertyName: string,
    columnName?: string
  ): string {
    return _.snakeCase(
      tableName + '_' + (columnName ? columnName : propertyName)
    );
  }

  // cannot find in DefaultNamingStrategy or NamingStrategyInterface
  classTableInheritanceParentColumnName(
    parentTableName,
    parentTableIdPropertyName
  ) {
    return _.snakeCase(parentTableName + '_' + parentTableIdPropertyName);
  }

  eagerJoinRelationAlias(alias: string, propertyPath: string): string {
    return alias + '__' + propertyPath.replace('.', '_');
  }
}
