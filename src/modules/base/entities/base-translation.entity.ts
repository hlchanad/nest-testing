import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class BaseTranslationEntity {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  id: number;

  @Column()
  language: string;
}
