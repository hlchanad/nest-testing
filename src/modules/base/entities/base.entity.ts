import { Column, PrimaryGeneratedColumn, VersionColumn } from 'typeorm';

export class BaseEntity {
  @PrimaryGeneratedColumn('increment', { type: 'bigint' })
  id: number;

  @Column()
  status?: string;

  @Column()
  deletedAt?: Date;

  @Column()
  deletedBy?: number;

  @Column()
  createdAt?: Date;

  @Column()
  createdBy?: number;

  @Column()
  updatedAt?: Date;

  @Column()
  updatedBy?: number;

  @VersionColumn()
  version: number;
}
