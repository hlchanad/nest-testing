import { Serializable } from 'child_process';

export interface IAppError {
  message?: string;
  stacktrace?: Serializable;
}
