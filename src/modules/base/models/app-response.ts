import { Serializable } from 'child_process';

import { IAppError } from './app-error';

export interface IAppResponse<T extends Serializable> {
  status: {
    code: string;
    message: string;
    detail?: string;
  };
  data?: T;
  errors?: IAppError[];
}

export class AppResponse<T extends Serializable> {
  constructor(
    private code: string,
    private message: string,
    private data?: T,
    private detail?: string,
    private errors?: IAppError[]
  ) {}

  toJSON(): IAppResponse<T> {
    return {
      status: {
        code: this.code,
        message: this.message,
        detail: this.detail,
      },
      data: this.data,
      errors: this.errors,
    };
  }

  clone<S extends Serializable>(): AppResponse<S> {
    return new AppResponse(this.code, this.message, null, this.detail);
  }

  setDetail(detail: string): AppResponse<T> {
    this.detail = detail;
    return this;
  }

  setData(data: T): AppResponse<T> {
    this.data = data;
    return this;
  }

  setError(errors: IAppError[]): AppResponse<T> {
    this.errors = errors;
    return this;
  }
}
