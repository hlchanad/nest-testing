import { Repository } from 'typeorm';

export abstract class BaseRepository<T> {
  protected repository: Repository<T>;

  save(t: T): Promise<T> {
    if (t['id'] === undefined) {
      this.setCreatedAt(t);
      this.setCreatedBy(t);
    } else {
      this.setUpdatedAt(t);
      this.setUpdatedBy(t);
    }

    return this.repository.save(t);
  }

  async delete(
    t: T,
    { hardDelete = false }: { hardDelete?: boolean } = {}
  ): Promise<void> {
    if (hardDelete) {
      // TODO: cascade?
      await this.repository.delete(t['id']);
    } else {
      this.setDeletedAt(t);
      this.setDeletedBy(t);

      await this.repository.save(t);
    }
  }

  private setCreatedAt(t: T) {
    if (t.hasOwnProperty('createdAt')) {
      t['createdAt'] = new Date();
    }
  }

  private setCreatedBy(t: T) {
    if (t.hasOwnProperty('createdBy')) {
      // TODO: get current user from context
      t['createdBy'] = null;
    }
  }

  private setUpdatedAt(t: T) {
    if (t.hasOwnProperty('updatedAt')) {
      t['updatedAt'] = new Date();
    }
  }

  private setUpdatedBy(t: T) {
    if (t.hasOwnProperty('updatedBy')) {
      // TODO: get current user from context
      t['updatedBy'] = null;
    }
  }

  private setDeletedAt(t: T) {
    if (t.hasOwnProperty('deletedAt')) {
      t['deletedAt'] = new Date();
    }
  }

  private setDeletedBy(t: T) {
    if (t.hasOwnProperty('deletedBy')) {
      // TODO: get current user from context
      t['deletedBy'] = null;
    }
  }
}
