export type RequiredIfTrue<TCondition, TRequired> =
  | (Required<Record<keyof TCondition, true>> & TRequired)
  | (Required<Record<keyof TCondition, false>> &
      Partial<Record<keyof TRequired, never>>);
