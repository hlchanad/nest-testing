interface RenderBaseOptions {
  data?: any;
}

interface RenderOptionsWithTemplate extends RenderBaseOptions {
  template: string;
  path?: never;
}

interface RenderOptionsWithPath extends RenderBaseOptions {
  path: string;
  template?: never;
}

export type RenderOptions = RenderOptionsWithTemplate | RenderOptionsWithPath;
