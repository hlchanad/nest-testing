import * as fs from 'fs';
import * as Handlebars from 'handlebars';

import { BaseService } from '../../../services';

import { RenderOptions } from '../constants';

export class TemplateService extends BaseService {
  static render(options: RenderOptions): string {
    const template = options.template || fs.readFileSync(options.path, 'utf8');

    return Handlebars.compile(template)(options.data);
  }
}
