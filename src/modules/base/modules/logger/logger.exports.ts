import { DynamicModule } from '@nestjs/common';

import { Logger } from './logger';
import { NestLogger } from './nest-logger';
import {
  LOGGER_MODULE_LOGGER,
  LOGGER_MODULE_NEST_LOGGER,
  LOGGER_MODULE_OPTIONS,
} from './logger.constants';

export function getExports(): DynamicModule['exports'] {
  return [Logger, LOGGER_MODULE_LOGGER, NestLogger, LOGGER_MODULE_NEST_LOGGER];
}

export function getExportsAsync(): DynamicModule['exports'] {
  return [
    LOGGER_MODULE_OPTIONS,
    Logger,
    LOGGER_MODULE_LOGGER,
    NestLogger,
    LOGGER_MODULE_NEST_LOGGER,
  ];
}
