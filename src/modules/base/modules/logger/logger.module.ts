import { DynamicModule, Global, Module } from '@nestjs/common';

import {
  LoggerModuleAsyncOptions,
  LoggerModuleOptions,
} from './logger.interfaces';
import { createAsyncProviders, createProviders } from './logger.providers';
import { getExports, getExportsAsync } from './logger.exports';

@Global()
@Module({})
export class LoggerModule {
  static register(options: LoggerModuleOptions): DynamicModule {
    return {
      module: LoggerModule,
      providers: createProviders(options),
      exports: getExports(),
    };
  }

  static registerAsync(options: LoggerModuleAsyncOptions): DynamicModule {
    return {
      module: LoggerModule,
      imports: options.imports,
      providers: createAsyncProviders(options),
      exports: getExportsAsync(),
    };
  }
}
