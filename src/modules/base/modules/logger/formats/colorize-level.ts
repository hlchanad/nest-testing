import * as deepMerge from 'deepmerge';
import * as colors from 'colors/safe';
import { TransformableInfo } from 'logform';

declare type ColorFn = (string) => string;

interface ColorizeLevelOptions {
  colors?: {
    [logLevel: string]: ColorFn;
  };
}

export class ColorizeLevel {
  opts: ColorizeLevelOptions;

  constructor(opts?: ColorizeLevelOptions) {
    this.opts = deepMerge(
      {
        colors: {
          debug: colors.blue,
          verbose: colors.cyan,
          info: colors.green,
          warn: colors.yellow,
          error: colors.red,
        },
      },
      opts || {}
    );
  }

  transform(info: TransformableInfo): TransformableInfo {
    return {
      ...info,
      level: this.colorize(info.level),
      timestamp: colors.gray(info.timestamp),
      context: colors.yellow(info.context),
    };
  }

  colorize(level: string): string {
    const colorFn = Object.entries(this.opts.colors).find((entry) =>
      level.startsWith(entry[0])
    )[1];
    return colorFn(level);
  }
}
