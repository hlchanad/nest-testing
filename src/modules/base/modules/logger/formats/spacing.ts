import * as _ from 'lodash';
import * as deepMerge from 'deepmerge';
import { TransformableInfo } from 'logform';

interface SpacingOptions {
  length?: {
    level?: number;
    context?: number;
    threadId?: number;
  };
}

export class Spacing {
  opts: SpacingOptions;

  constructor(opts?: SpacingOptions) {
    this.opts = deepMerge(
      {
        length: {
          level: 7,
          context: 25,
          threadId: 6,
        },
      },
      opts || {}
    );
  }

  transform(info: TransformableInfo): TransformableInfo {
    return {
      ...info,
      level: _.padEnd(info.level, this.opts.length.level),
      context: _.padStart(info.context, this.opts.length.context),
      threadId: _.padStart(info.threadId, this.opts.length.threadId),
    };
  }
}
