import { Provider } from '@nestjs/common';

import {
  LoggerModuleAsyncOptions,
  LoggerModuleOptions,
} from './logger.interfaces';
import {
  LOGGER_MODULE_LOGGER,
  LOGGER_MODULE_LOGGER_DRIVER,
  LOGGER_MODULE_NEST_LOGGER,
  LOGGER_MODULE_OPTIONS,
} from './logger.constants';
import { Logger } from './logger';
import { NestLogger } from './nest-logger';
import { createWinstonLogger } from './winston-logger';

export function createProviders(options: LoggerModuleOptions): Provider[] {
  return [
    {
      provide: LOGGER_MODULE_LOGGER_DRIVER,
      useValue: createWinstonLogger(options),
    },
    Logger,
    {
      provide: LOGGER_MODULE_LOGGER,
      useClass: Logger,
    },
    NestLogger,
    {
      provide: LOGGER_MODULE_NEST_LOGGER,
      useClass: NestLogger,
    },
  ];
}

export function createAsyncProviders(
  options: LoggerModuleAsyncOptions
): Provider[] {
  return [
    {
      provide: LOGGER_MODULE_OPTIONS,
      useFactory: options.useFactory,
      inject: options.inject,
    },
    {
      provide: LOGGER_MODULE_LOGGER_DRIVER,
      inject: [LOGGER_MODULE_OPTIONS],
      useFactory: (loggerModuleOptions: LoggerModuleOptions) =>
        createWinstonLogger(loggerModuleOptions),
    },
    Logger,
    {
      provide: LOGGER_MODULE_LOGGER,
      useClass: Logger,
    },
    NestLogger,
    {
      provide: LOGGER_MODULE_NEST_LOGGER,
      useClass: NestLogger,
    },
  ];
}
