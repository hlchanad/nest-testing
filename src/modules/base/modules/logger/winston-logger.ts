import * as _ from 'lodash';
import * as winston from 'winston';

import { LoggerModuleOptions } from './logger.interfaces';
import { ColorizeLevel, Spacing } from './formats';

export function createWinstonLogger(
  options: LoggerModuleOptions
): winston.Logger {
  const format = winston.format.combine(
    winston.format.simple(),
    winston.format.timestamp(),
    new Spacing(),
    new ColorizeLevel(),
    winston.format.printf((props) => {
      const { context, level, message, timestamp, threadId } = props;
      return `${timestamp} | ${level} | ${threadId} | ${context} | ${message}`;
    })
  );

  return winston.createLogger({
    silent: !_.get(options, 'isEnabled', true),
    level: _.get(options, 'logLevel', 'info'),
    transports: [new winston.transports.Console({ format })],
  });
}
