import {
  Inject,
  Injectable,
  LoggerService as NestLoggerService,
} from '@nestjs/common';

import { LOGGER_MODULE_LOGGER } from './logger.constants';
import { Logger } from './logger';

@Injectable()
export class NestLogger implements NestLoggerService {
  constructor(@Inject(LOGGER_MODULE_LOGGER) private readonly logger: Logger) {}

  debug(message: any, context?: string): any {
    this.logger.setContext(context || '').debug(message);
  }

  error(message: any, trace?: string, context?: string): any {
    this.logger.setContext(context || '').error(message, trace);
  }

  log(message: any, context?: string): any {
    this.logger.setContext(context || '').info(message);
  }

  verbose(message: any, context?: string): any {
    this.logger.setContext(context || '').verbose(message);
  }

  warn(message: any, context?: string): any {
    this.logger.setContext(context || '').warn(message);
  }
}
