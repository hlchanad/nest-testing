import { Injectable, Module } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import 'jest-extended';

import { LoggerModule } from './logger.module';
import { Logger } from './logger';
import { LOGGER_MODULE_LOGGER } from './logger.constants';
import { LoggerModuleOptions } from './logger.interfaces';

describe('Logger Module', () => {
  it('boots successfully', async () => {
    const rootModule = await Test.createTestingModule({
      imports: [LoggerModule.register({})],
    }).compile();

    const logger = await rootModule.resolve(Logger);
    const nestLogger = await rootModule.resolve(LOGGER_MODULE_LOGGER);

    expect(logger).toBeObject();
    expect(nestLogger).toBeObject();
  });

  it('boots successfully asynchronously via useFactory', async () => {
    @Injectable()
    class ConfigService {
      public loggerOptions: LoggerModuleOptions = {};
    }

    @Module({
      providers: [ConfigService],
      exports: [ConfigService],
    })
    class ConfigModule {}

    const rootModule = await Test.createTestingModule({
      imports: [
        LoggerModule.registerAsync({
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (configService: ConfigService) =>
            configService.loggerOptions,
        }),
      ],
    }).compile();

    await rootModule.createNestApplication().init();

    const logger = await rootModule.resolve(Logger);
    const nestLogger = await rootModule.resolve(LOGGER_MODULE_LOGGER);

    expect(logger).toBeObject();
    expect(nestLogger).toBeObject();
  });
});
