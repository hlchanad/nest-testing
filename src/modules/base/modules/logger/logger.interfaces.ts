import { ModuleMetadata } from '@nestjs/common/interfaces';

export type LogLevel = 'debug' | 'verbose' | 'info' | 'warn' | 'error';

export interface LoggerModuleOptions {
  isEnabled?: boolean;
  logLevel?: LogLevel;
}

export interface LoggerModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => Promise<LoggerModuleOptions> | LoggerModuleOptions;
  inject: any[];
}
