export const LOGGER_MODULE_OPTIONS = 'logger-module:options';
export const LOGGER_MODULE_LOGGER_DRIVER = 'logger-module:logger-driver';
export const LOGGER_MODULE_LOGGER = 'logger-module:logger';
export const LOGGER_MODULE_NEST_LOGGER = 'logger-module:nest-logger';
