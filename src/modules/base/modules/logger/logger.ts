import { Inject, Injectable, Scope } from '@nestjs/common';
import { Serializable } from 'child_process';
import { Logger as WinstonLogger } from 'winston';

import { asyncHook } from '../base';
import { LOGGER_MODULE_LOGGER_DRIVER } from './logger.constants';

export interface LoggerService {
  info(message: string, data?: Serializable): void;
  error(message: any, trace?: Serializable): void;
  warn(message: any, data?: Serializable): void;
  debug?(message: any, data?: Serializable): void;
  verbose?(message: any, data?: Serializable): void;
}

export function getLogLine(message: string, data?: Serializable): string {
  let line = message;
  if (data) {
    line += ' - ' + JSON.stringify(data, null, 2);
  }
  return line;
}

@Injectable({ scope: Scope.TRANSIENT })
export class Logger implements LoggerService {
  private context?: string;

  constructor(
    @Inject(LOGGER_MODULE_LOGGER_DRIVER) private readonly logger: WinstonLogger
  ) {}

  setContext(context: string): Logger {
    this.context = context;
    return this;
  }

  info(message: string, data?: Serializable): void {
    this.logger.info(getLogLine(message, data), {
      ...this.getCommonMetas(),
    });
  }

  error(message: string, trace?: Serializable): void {
    this.logger.error(getLogLine(message, trace), {
      ...this.getCommonMetas(),
      tract: trace,
    });
  }

  warn(message: string, data?: Serializable): void {
    this.logger.warn(getLogLine(message, data), {
      ...this.getCommonMetas(),
    });
  }

  debug(message: string, data?: Serializable): void {
    this.logger.debug(getLogLine(message, data), {
      ...this.getCommonMetas(),
    });
  }

  verbose(message: string, data?: Serializable): void {
    this.logger.verbose(getLogLine(message, data), {
      ...this.getCommonMetas(),
    });
  }

  private getCommonMetas(): object {
    return {
      context: this.context,
      threadId: asyncHook.get()?.id,
    };
  }
}
