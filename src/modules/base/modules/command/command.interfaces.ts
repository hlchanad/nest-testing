import { ModuleMetadata } from '@nestjs/common/interfaces';
import { CommandModule } from 'yargs';

export type Command = CommandModule;

export interface CommandModuleOptions {}

export interface CommandModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => Promise<CommandModuleOptions> | CommandModuleOptions;
  inject: any[];
}
