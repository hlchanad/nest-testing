import { Injectable } from '@nestjs/common';

import { HelpCommand } from './commands';
import { Command } from './command.interfaces';

@Injectable()
export class CommandService {
  private commands: Command[] = [HelpCommand()];

  register(command: Command) {
    this.commands.push(command);
  }

  list(): Command[] {
    return [...this.commands];
  }
}
