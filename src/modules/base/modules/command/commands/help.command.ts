import * as yargs from 'yargs';

import { Command } from '../command.interfaces';

export function HelpCommand(): Command {
  return {
    command: 'help',
    describe: 'Show help',
    handler: (args) => {
      yargs.showHelp();
    },
  };
}
