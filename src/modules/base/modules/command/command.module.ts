import { DynamicModule, Global, Module } from '@nestjs/common';

import {
  CommandModuleAsyncOptions,
  CommandModuleOptions,
} from './command.interfaces';
import { COMMAND_MODULE_OPTIONS } from './command.constants';
import { CommandService } from './command.service';

@Global()
@Module({})
export class CommandModule {
  static register(options: CommandModuleOptions): DynamicModule {
    return {
      module: CommandModule,
      providers: [
        {
          provide: COMMAND_MODULE_OPTIONS,
          useValue: options,
        },
        CommandService,
      ],
      exports: [CommandService],
    };
  }

  static registerAsync(options: CommandModuleAsyncOptions): DynamicModule {
    return {
      module: CommandModule,
      imports: options.imports,
      providers: [
        {
          provide: COMMAND_MODULE_OPTIONS,
          useFactory: options.useFactory,
          inject: options.inject,
        },
        CommandService,
      ],
      exports: [CommandService],
    };
  }
}
