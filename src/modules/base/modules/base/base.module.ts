import {
  DynamicModule,
  Global,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { LoggerMiddleware, SetAsyncHookMiddleware } from './middlewares';
import { AllExceptionsFilter } from './filters';
import { LoggerModule } from '../logger';
import { MigrationModule } from '../migration';
import { CommandService } from '../command';
import { BaseModuleAsyncOptions, BaseModuleOptions } from './base.interfaces';
import { BASE_MODULE_OPTIONS } from './base.constants';
import { ValidatorModule } from '@src/modules/base/modules/validator/validator.module';

@Global()
@Module({})
export class BaseModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(SetAsyncHookMiddleware).forRoutes('*');
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }

  static register(options: BaseModuleOptions): DynamicModule {
    const { imports, providers, exports } = this.registerModules([
      LoggerModule.register(options.loggerModuleOptions || {}),
      MigrationModule.register(options.migrationModuleOptions),
      // CommandModule.register(options.commandModuleOptions || {}),
      ValidatorModule.register(options.validatorModuleOptions || {}),
    ]);

    return {
      module: BaseModule,
      imports: imports,
      providers: [
        ...providers,
        {
          provide: BASE_MODULE_OPTIONS,
          useValue: options,
        },
        {
          provide: APP_FILTER,
          useClass: AllExceptionsFilter,
        },
        CommandService,
      ],
      exports: [...exports, CommandService],
    };
  }

  static registerAsync(options: BaseModuleAsyncOptions): DynamicModule {
    const { imports, providers, exports } = this.registerModules([
      LoggerModule.registerAsync({
        useFactory: (baseModuleOptions: BaseModuleOptions) =>
          baseModuleOptions.loggerModuleOptions || {},
        inject: [BASE_MODULE_OPTIONS],
      }),
      MigrationModule.registerAsync({
        useFactory: (baseModuleOptions: BaseModuleOptions) =>
          baseModuleOptions.migrationModuleOptions,
        inject: [BASE_MODULE_OPTIONS],
      }),
      // CommandModule.registerAsync({
      //   useFactory: (baseModuleOptions: BaseModuleOptions) =>
      //     baseModuleOptions.commandModuleOptions || {},
      //   inject: [BASE_MODULE_OPTIONS],
      // }),
      ValidatorModule.registerAsync({
        useFactory: (baseModuleOptions: BaseModuleOptions) =>
          baseModuleOptions.validatorModuleOptions || {},
        inject: [BASE_MODULE_OPTIONS],
      }),
    ]);

    return {
      module: BaseModule,
      imports: [...options.imports, ...imports],
      providers: [
        ...providers,
        {
          provide: BASE_MODULE_OPTIONS,
          useFactory: options.useFactory,
          inject: options.inject,
        },
        {
          provide: APP_FILTER,
          useClass: AllExceptionsFilter,
        },
        CommandService,
      ],
      exports: [...exports, BASE_MODULE_OPTIONS, CommandService],
    };
  }

  private static registerModules(
    modules: DynamicModule[]
  ): Pick<DynamicModule, 'imports' | 'providers' | 'exports'> {
    const [imports, providers, exports] = [[], [], []];

    modules.forEach((module) => {
      imports.push(module);
      providers.push(...(module.providers || []));
      exports.push(...(module.exports || []));
    });

    return { imports, providers, exports };
  }
}
