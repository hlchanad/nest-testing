import {
  Inject,
  Injectable,
  LoggerService,
  NestMiddleware,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

import { LOGGER_MODULE_NEST_LOGGER, getLogLine, LogLevel } from '../../logger';
import { BASE_MODULE_OPTIONS } from '../base.constants';
import { BaseModuleOptions } from '../base.interfaces';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private readonly options: BaseModuleOptions['loggerMiddlewareOptions'];

  constructor(
    // no idea why my logger does not work...
    @Inject(LOGGER_MODULE_NEST_LOGGER) private readonly logger: LoggerService,
    @Inject(BASE_MODULE_OPTIONS) baseModuleOptions: BaseModuleOptions
  ) {
    this.options = baseModuleOptions.loggerMiddlewareOptions || {};
  }

  async use(request: Request, response: Response, next: NextFunction) {
    if (!this.doFilter(request)) return next();

    const log = this.log.bind(this);

    const skipHeaders = this.options.excludeHeaders || [];
    const headers = Object.entries(request.headers)
      .map((entry) =>
        skipHeaders.includes(entry[0]) ? [entry[0], 'skipped ...'] : entry
      )
      .reduce((acc, entry) => ({ ...acc, [entry[0]]: entry[1] }), {});

    log(`request endpoint: ${request.originalUrl}`);
    log(`request headers`, headers, 'verbose');
    log(`request queries`, request.query, 'verbose');
    log(`request body`, request.body, 'verbose');

    await next();

    // https://stackoverflow.com/questions/19215042/express-logging-response-body
    const oldSend = response.send;
    (response.send as unknown) = function (body) {
      log(`response headers`, response.getHeaders(), 'verbose');
      log(`response body`, LoggerMiddleware.getJsonBody(body), 'verbose');
      oldSend.call(this, body);
    };
  }

  private doFilter(request: Request): boolean {
    return !(this.options.excludeRoutes || []).some(
      (route) =>
        route.method === request.method &&
        route.endpoint === request.originalUrl
    );
  }

  private log(message: string, props?: any, level: LogLevel = 'info'): void {
    const logFn = this.getLogFunction(level).bind(this.logger);
    logFn(getLogLine(message, props), LoggerMiddleware.name);
  }

  private getLogFunction(level: LogLevel = 'info') {
    switch (level) {
      case 'debug':
        return this.logger.debug;
      case 'verbose':
        return this.logger.verbose;
      case 'info':
        return this.logger.log;
      case 'warn':
        return this.logger.warn;
      case 'error':
        return this.logger.error;
    }
  }

  private static getJsonBody(body: string): object | string {
    try {
      return JSON.parse(body);
    } catch (e) {
      return body;
    }
  }
}
