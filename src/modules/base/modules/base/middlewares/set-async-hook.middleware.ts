import { Inject, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

import { asyncHook } from '../helpers';
import { BASE_MODULE_OPTIONS } from '../base.constants';
import { BaseModuleOptions } from '../base.interfaces';

@Injectable()
export class SetAsyncHookMiddleware implements NestMiddleware {
  private readonly requestIdFn: (request: Request) => string;

  constructor(
    @Inject(BASE_MODULE_OPTIONS) baseModuleOptions: BaseModuleOptions
  ) {
    this.requestIdFn = baseModuleOptions.requestIdFn;
  }

  use(request: Request, response: Response, next: NextFunction): any {
    const threadId = this.requestIdFn?.(request)?.toString();

    asyncHook.set(request, threadId);

    next();
  }
}
