import * as _ from 'lodash';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Inject,
} from '@nestjs/common';

import { Logger, LOGGER_MODULE_LOGGER } from '../../logger';
import { AppException } from '../../../exceptions';
import { IAppResponse } from '../../../models';
import { BaseAppResponses } from '../../../constants';
import { BASE_MODULE_OPTIONS } from '../base.constants';
import { BaseModuleOptions } from '../base.interfaces';

interface ExceptionHandler<T extends Error> {
  identifier: string;
  exception: T;
  condition?: (exception: T) => boolean;
  handler: (
    exception: T
  ) => { httpStatus: HttpStatus; jsonResponse: IAppResponse<any> };
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private options: BaseModuleOptions['exceptionFilterOptions'];
  private handlers: ExceptionHandler<any>[] =
    AllExceptionsFilter.DEFAULT_EXCEPTION_HANDLERS;

  constructor(
    @Inject(LOGGER_MODULE_LOGGER) private readonly logger: Logger,
    @Inject(BASE_MODULE_OPTIONS) baseModuleOptions: BaseModuleOptions
  ) {
    this.logger.setContext(AllExceptionsFilter.name);
    this.options = baseModuleOptions.exceptionFilterOptions || {};

    if (this.options.start) {
      this.options.start(this);
    }
  }

  catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    const exceptionHandler = this.handlers.find(
      (handler) =>
        exception instanceof handler.exception &&
        (handler.condition == null ||
          (typeof handler.condition === 'function' &&
            handler.condition(exception)))
    );

    // if handled in the defined handlers
    if (exceptionHandler) {
      const { httpStatus, jsonResponse } = exceptionHandler.handler(exception);
      return response.status(httpStatus).send(jsonResponse);
    }

    // catch any other strange errors
    else {
      this.logger.error(exception.message, exception.stack);
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).send(
        BaseAppResponses.SomethingWentWrong.clone()
          .setError([
            {
              message: exception.message,
              stacktrace: exception.stack,
            },
          ])
          .toJSON()
      );
    }
  }

  static get DEFAULT_EXCEPTION_HANDLERS(): ExceptionHandler<any>[] {
    return [
      {
        identifier: 'app-exception',
        exception: AppException,
        handler: (exception: AppException) => ({
          httpStatus: exception.statusCode(),
          jsonResponse: exception.serializeError(),
        }),
      },
      {
        identifier: 'http-exception.bad-request',
        exception: HttpException,
        condition: (exception: HttpException) =>
          exception.getStatus() === HttpStatus.BAD_REQUEST,
        handler: (exception: HttpException) => ({
          httpStatus: exception.getStatus(),
          jsonResponse: BaseAppResponses.InvalidInput.clone()
            .setError(
              (exception.getResponse() as {
                message: string[];
              }).message.map((message) => ({ message }))
            )
            .toJSON(),
        }),
      },
    ];
  }

  insertBefore(
    identifier: string,
    exceptionHandler: ExceptionHandler<any>
  ): void {
    const index = _.findIndex(this.handlers, { identifier });

    if (index < 0) return;

    this.handlers.splice(index, 0, exceptionHandler);
  }

  insertAfter(
    identifier: string,
    exceptionHandler: ExceptionHandler<any>
  ): void {
    const index = _.findIndex(this.handlers, { identifier });

    if (index < 0) return;

    this.handlers.splice(index + 1, 0, exceptionHandler);
  }

  remove(identifier: string): void {
    const index = _.findIndex(this.handlers, { identifier });

    if (index < 0) return;

    this.handlers.splice(index, 1);
  }
}
