// Reference: https://stackabuse.com/using-async-hooks-for-request-context-handling-in-node-js/

import * as asyncHooks from 'async_hooks';
import { v4 as uuidV4 } from 'uuid';

const store = new Map();

const asyncHook = asyncHooks.createHook({
  init: (asyncId, _, triggerAsyncId) => {
    if (store.has(triggerAsyncId)) {
      store.set(asyncId, store.get(triggerAsyncId));
    }
  },
  destroy: (asyncId) => {
    if (store.has(asyncId)) {
      store.delete(asyncId);
    }
  },
});

asyncHook.enable();

export function set(data: any, id: string = uuidV4()) {
  store.set(asyncHooks.executionAsyncId(), { data, id });
}

export function get() {
  return store.get(asyncHooks.executionAsyncId());
}
