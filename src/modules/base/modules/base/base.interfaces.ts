import { ModuleMetadata } from '@nestjs/common/interfaces';
import { ConnectionOptions } from 'typeorm';
import { Request } from 'express';

import { LoggerModuleOptions as LoggerOptions } from '../logger';
import { CommandModuleOptions as CommandOptions } from '../command';
import { ValidatorModuleOptions as ValidatorOptions } from '../validator';
import { AllExceptionsFilter } from './filters';

interface BaseOptions {
  requestIdFn?: (request: Request) => string;
}

interface LoggerModuleOptions {
  loggerModuleOptions?: LoggerOptions;
}

interface ExceptionFilterOptions {
  exceptionFilterOptions?: {
    start?: (exceptionFilter: AllExceptionsFilter) => void;
  };
}

type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';
interface LoggerMiddlewareOptions {
  loggerMiddlewareOptions?: {
    excludeHeaders?: string[];
    excludeRoutes?: { method: HttpMethod; endpoint: string }[];
  };
}

interface MigrationModuleOptions {
  migrationModuleOptions: ConnectionOptions;
}

interface CommandModuleOptions {
  commandModuleOptions?: CommandOptions;
}

interface ValidatorModuleOptions {
  validatorModuleOptions?: ValidatorOptions;
}

export type BaseModuleOptions = BaseOptions &
  LoggerModuleOptions &
  ExceptionFilterOptions &
  LoggerMiddlewareOptions &
  MigrationModuleOptions &
  CommandModuleOptions &
  ValidatorModuleOptions;

export interface BaseModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => Promise<BaseModuleOptions> | BaseModuleOptions;
  inject: any[];
}
