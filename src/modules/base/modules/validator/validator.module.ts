import { DynamicModule, Module } from '@nestjs/common';

import {
  ValidatorModuleAsyncOptions,
  ValidatorModuleOptions,
} from './interfaces';
import { VALIDATOR_MODULE_OPTIONS } from './constants';
import { ValidatorService } from './validator.service';
import * as rules from './rules';

@Module({})
export class ValidatorModule {
  constructor(private readonly validatorService: ValidatorService) {
    this.validatorService.initialize();
    Object.entries(rules).forEach(([name, validation]) =>
      this.validatorService.extend(name, validation)
    );
  }

  static register(options: ValidatorModuleOptions): DynamicModule {
    return {
      module: ValidatorModule,
      providers: [
        {
          provide: VALIDATOR_MODULE_OPTIONS,
          useValue: options,
        },
        ValidatorService,
      ],
      exports: [ValidatorService],
    };
  }

  static registerAsync(options: ValidatorModuleAsyncOptions): DynamicModule {
    return {
      module: ValidatorModule,
      imports: options.imports,
      providers: [
        {
          provide: VALIDATOR_MODULE_OPTIONS,
          useFactory: options.useFactory,
          inject: options.inject,
        },
        ValidatorService,
      ],
      exports: [ValidatorService],
    };
  }
}
