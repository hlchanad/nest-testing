export const ValidateMetadata = {
  HEADERS: 'base.@validate.headers',
  PARAMS: 'base.@validate.params',
  QUERY: 'base.@validate.query',
  BODY: 'base.@validate.body',
};

export const ValidationErrorKey = 'validationErrors';

export const VALIDATOR_MODULE_OPTIONS = 'validator-module:options';
