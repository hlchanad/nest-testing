import { ValidateMetadata } from '../constants';
import { Section, ValidatorOptions } from '../interfaces';

function getMetadataKey(section: Section): string {
  return {
    [Section.HEADERS]: ValidateMetadata.HEADERS,
    [Section.PARAMS]: ValidateMetadata.PARAMS,
    [Section.QUERY]: ValidateMetadata.QUERY,
    [Section.BODY]: ValidateMetadata.BODY,
  }[section];
}

function common(section: Section) {
  return (
    schema: object,
    messages?: object,
    options?: ValidatorOptions
  ): MethodDecorator => {
    return (target, key, descriptor) => {
      const data = { schema, messages, options };
      Reflect.defineMetadata(getMetadataKey(section), data, descriptor.value);
    };
  };
}

export const Validate = {
  Headers: common(Section.HEADERS),
  Params: common(Section.PARAMS),
  Query: common(Section.QUERY),
  Body: common(Section.BODY),
};
