import { getValue, skippable } from 'indicative-utils';

export const testAsync = {
  async: true,
  compile(args) {
    // checking on args goes here
    return args;
  },
  async validate(data, field, args, config) {
    const value = getValue(data, field);

    if (skippable(value, field, config)) {
      return true;
    }

    await new Promise((resolve) => setTimeout(resolve, 2000));

    if (value === 'error') {
      return false;
    }

    return true;
  },
};
