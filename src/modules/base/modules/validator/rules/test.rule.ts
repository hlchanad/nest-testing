import { getValue, skippable } from 'indicative-utils';

export const test = {
  async: false,
  compile(args) {
    // checking on args goes here
    return args;
  },
  validate(data, field, args, config: any) {
    const value = getValue(data, field);

    if (skippable(value, field, config)) {
      return true;
    }

    if (value === 'error') {
      return false;
    }

    return true;
  },
};
