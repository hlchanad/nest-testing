import {
  HttpStatus,
  Inject,
  Injectable,
  MiddlewareConsumer,
  RequestMethod,
} from '@nestjs/common';
import { METHOD_METADATA, PATH_METADATA } from '@nestjs/common/constants';
import { configure, extend, validateAll } from 'indicative/validator';
import { NextFunction, Request, Response } from 'express';
import * as _ from 'lodash';

import { AppException } from '../../exceptions';
import { BaseAppResponses } from '../../constants';

import {
  ValidateMetadata,
  ValidationErrorKey,
  VALIDATOR_MODULE_OPTIONS,
} from './constants';
import {
  Section,
  ValidatorModuleOptions,
  ValidatorOptions,
} from './interfaces';

@Injectable()
export class ValidatorService {
  constructor(
    @Inject(VALIDATOR_MODULE_OPTIONS)
    private readonly options: ValidatorModuleOptions
  ) {
    this.options = Object.assign(
      {},
      ValidatorService.DEFAULT_CONFIG,
      this.options
    );
  }

  private static get DEFAULT_CONFIG(): ValidatorModuleOptions {
    return {
      removeAdditional: true,
    };
  }

  initialize() {
    configure(this.options);
  }

  registerMiddlewares(consumer: MiddlewareConsumer, controllers: any[]) {
    controllers.forEach((controller) => {
      const controllerPath = Reflect.getMetadata(PATH_METADATA, controller);
      const subPaths = this.scanDecorator(controller);

      subPaths.forEach((path) => {
        const forRoutes = {
          path: controllerPath + path.path,
          method: path.method,
        };

        path.validations.forEach((validation) =>
          consumer
            .apply(
              validation.validator(
                validation.schema,
                validation.messages,
                validation.options
              )
            )
            .forRoutes(forRoutes)
        );

        consumer.apply(this.collectErrors()).forRoutes(forRoutes);
      });
    });
  }

  extend = extend;

  private collectErrors() {
    return async (request: Request, response: Response, next: NextFunction) => {
      const errors: {
        message: string;
        validation: string;
        field: string;
      }[] = _.flatten(
        Object.values(response.locals?.[ValidationErrorKey] || {})
      );

      if (!_.isEmpty(errors)) {
        throw new AppException(
          HttpStatus.BAD_REQUEST,
          BaseAppResponses.InvalidInput.clone().setError(errors)
        );
      }

      next();
    };
  }

  private headers() {
    return this.commonMiddleware(Section.HEADERS, { removeAdditional: false });
  }

  private params() {
    return this.commonMiddleware(Section.PARAMS);
  }

  private query() {
    return this.commonMiddleware(Section.QUERY);
  }

  private body() {
    return this.commonMiddleware(Section.BODY);
  }

  private commonMiddleware(
    section: Section,
    defaultOptions?: ValidatorOptions
  ) {
    return (schema, messages?, options: ValidatorOptions = defaultOptions) => {
      return async (
        request: Request,
        response: Response,
        next: NextFunction
      ) => {
        options = Object.assign({}, this.options, options);

        try {
          request[section] = await validateAll(
            request[section],
            schema,
            messages,
            {
              ...options,
              cacheKey:
                request.method + '.' + request.originalUrl + '.' + section,
            }
          );
        } catch (error) {
          response.locals = {
            ...(response.locals || {}),
            [ValidationErrorKey]: {
              ...(response.locals?.[ValidationErrorKey] || {}),
              [section]: error,
            },
          };
        }

        next();
      };
    };
  }

  private scanDecorator(
    controller
  ): {
    path: string;
    method: RequestMethod;
    methodName: string;
    fn: Function;
    validations: {
      schema: object;
      messages?: object;
      options?: ValidatorOptions;
      validator: Function;
    }[];
  }[] {
    const prototype = controller.prototype;

    return Object.getOwnPropertyNames(controller.prototype)
      .filter(
        (methodName) =>
          methodName !== 'constructor' &&
          typeof prototype[methodName] === 'function'
      )
      .map((methodName) => ({ methodName, fn: prototype[methodName] }))
      .map(({ methodName, fn }) => ({
        path: Reflect.getMetadata(PATH_METADATA, fn),
        method: Reflect.getMetadata(METHOD_METADATA, fn),
        methodName,
        fn,
        validations: [...this.DECORATOR_MAPPING]
          .filter(({ metadata }) => Reflect.getMetadata(metadata, fn))
          .map(({ metadata, fn: validator }) => ({
            ...Reflect.getMetadata(metadata, fn),
            validator,
          })),
      }))
      .filter((result) => result.validations.length > 0);
  }

  private get DECORATOR_MAPPING(): {
    metadata: string;
    fn: Function;
  }[] {
    return [
      { metadata: ValidateMetadata.HEADERS, fn: this.headers() },
      { metadata: ValidateMetadata.PARAMS, fn: this.params() },
      { metadata: ValidateMetadata.QUERY, fn: this.query() },
      { metadata: ValidateMetadata.BODY, fn: this.body() },
    ];
  }
}
