import { ValidatorConfig } from 'indicative/src/Contracts';

export type ValidatorOptions = Partial<ValidatorConfig>;

export enum Section {
  HEADERS = 'headers',
  PARAMS = 'params',
  QUERY = 'query',
  BODY = 'body',
}

import { ModuleMetadata } from '@nestjs/common/interfaces';

export interface ValidatorModuleOptions extends Partial<ValidatorOptions> {}

export interface ValidatorModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => Promise<ValidatorModuleOptions> | ValidatorModuleOptions;
  inject: any[];
}
