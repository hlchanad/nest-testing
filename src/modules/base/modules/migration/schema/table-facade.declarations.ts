import { ColumnBuilder } from './builders';

declare module './table-facade' {
  interface TableFacade {
    /* Normal Data Types */
    varchar(name: string, length?: number): ColumnBuilder;
    text(name: string): ColumnBuilder;
    int(name: string): ColumnBuilder;
    bigInt(name: string): ColumnBuilder;
    decimal(name: string, total: number, decimal: number): ColumnBuilder;
    bit(name: string, length: number): ColumnBuilder;
    date(name: string): ColumnBuilder;
    datetime(name: string): ColumnBuilder;
    enums(name: string, enums: string[], enumName?: string): ColumnBuilder;

    /* Custom Data Types */
    id(): ColumnBuilder;
    increments(name: string): ColumnBuilder;
    bigIncrements(name: string): ColumnBuilder;
    boolean(name: string): ColumnBuilder;

    /* Overrode Types */
    manageable(options?: { setFK: boolean }): ColumnBuilder[];
    refId(foreignTable: string, name?: string): ColumnBuilder;
    language(): ColumnBuilder;
  }
}
