import { TypeormTable } from '../../migration.interfaces';
import { ColumnBuilder } from '../builders';
import { TableFacade } from '../table-facade';

describe('TableFacade', () => {
  it('builds a TypeormTable object', () => {
    const table = new TableFacade('table');
    const result = table.create(() => {});

    expect(result).toBeInstanceOf(TypeormTable);
  });

  describe('column()', () => {
    it('supports adding TypeormTableColumn column', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.column({ name: 'column', type: 'type' });
      });

      expect(result).toBeInstanceOf(TypeormTable);
      expect(result.columns[0].name).toBe('column');
      expect(result.columns[0].type).toBe('type');
    });
  });

  describe('ColumnType proxies', () => {
    it('proxies the columnTypes function', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        const chain = table.id();
        table.varchar('name', 1000).notNull();

        expect(chain).toBeInstanceOf(ColumnBuilder);
      });

      expect(result.columns[0].name).toBe('id');
      expect(result.columns[0].type).toBe('bigInt');
      expect(result.columns[0].isGenerated).toBeTruthy();
      expect(result.columns[0].generationStrategy).toBe('increment');
      expect(result.columns[0].isPrimary).toBeTruthy();
      expect(result.columns[0].isNullable).toBeFalsy();

      expect(result.columns[1].name).toBe('name');
      expect(result.columns[1].type).toBe('varchar');
      expect(result.columns[1].length).toBe((1000).toString());
      expect(result.columns[1].isNullable).toBeFalsy();
    });

    it('supports both ColumnBuilder and ColumnBuilder[]', () => {
      new TableFacade('table').create((table) => {
        table.varchar('varchar');
        expect(table['columns']).toHaveLength(1);

        table.manageable();
        expect(table['columns']).toHaveLength(9);
      });
    });
  });

  describe('overrode ColumnTypes', () => {
    it('sets up fk for manageable', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.manageable();
      });

      expect(result.columns).toHaveLength(8);
      expect(result.foreignKeys).toHaveLength(3);
    });

    it('does not set up fk for manageable', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.manageable({ setFK: false });
      });

      expect(result.columns).toHaveLength(8);
      expect(result.foreignKeys).toHaveLength(0);
    });

    it('sets up fk for refId', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.refId('articles');
      });

      expect(result.columns).toHaveLength(1);
      expect(result.foreignKeys).toHaveLength(1);
    });
  });

  describe('foriegnKey()', () => {
    function check(result: TypeormTable) {
      expect(result).toBeInstanceOf(TypeormTable);
      expect(result.foreignKeys).toHaveLength(1);
      expect(result.foreignKeys[0].name).toBe('fk');
      expect(result.foreignKeys[0].columnNames).toStrictEqual(['created_by']);
      expect(result.foreignKeys[0].referencedTableName).toBe('users');
      expect(result.foreignKeys[0].referencedColumnNames).toStrictEqual(['id']);
      expect(result.foreignKeys[0].onDelete).toStrictEqual('onDelete');
    }

    it('supports adding TypeormTableForeignKey fk', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.foreignKey({
          name: 'fk',
          columnNames: ['created_by'],
          referencedTableName: 'users',
          referencedColumnNames: ['id'],
          onDelete: 'onDelete',
        });
      });

      check(result);
    });

    it('supports adding ForeignKeyBuilder fk', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table
          .foreignKey('created_by', 'users', 'id', 'fk')
          .onDelete('onDelete');
      });

      check(result);
    });
  });

  describe('index()', () => {
    function check(result: TypeormTable) {
      expect(result).toBeInstanceOf(TypeormTable);
      expect(result.indices).toHaveLength(1);
      expect(result.indices[0].name).toBe('index');
      expect(result.indices[0].columnNames).toStrictEqual(['id']);
      expect(result.indices[0].isUnique).toBeTruthy();
    }

    it('supports adding TypeormTableIndex index', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.index({ name: 'index', columnNames: ['id'], isUnique: true });
      });

      check(result);
    });

    it('supports adding IndexBuilder index', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.index(['id'], 'index').unique();
      });

      check(result);
    });
  });

  describe('unique()', () => {
    function check(result: TypeormTable) {
      expect(result).toBeInstanceOf(TypeormTable);
      expect(result.uniques).toHaveLength(1);
      expect(result.uniques[0].name).toBe('unique');
      expect(result.uniques[0].columnNames).toStrictEqual(['id']);
    }

    it('supports adding TypeormTableUnique unique', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.unique({ name: 'unique', columnNames: ['id'] });
      });

      check(result);
    });

    it('supports adding UniqueBuilder unique', () => {
      const table = new TableFacade('table');
      const result = table.create((table) => {
        table.unique(['id'], 'unique');
      });

      check(result);
    });
  });
});
