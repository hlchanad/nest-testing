import {
  TypeormTable,
  TypeormTableColumnOptions,
  TypeormTableForeignKeyOptions,
  TypeormTableIndexOptions,
  TypeormTableUniqueOptions,
} from '../migration.interfaces';
import * as Columns from './column-types';
import {
  ColumnBuilder,
  ForeignKeyBuilder,
  IndexBuilder,
  TableBuilder,
  UniqueBuilder,
} from './builders';
import './table-facade.declarations';

type BuilderFn = (table: TableFacade) => void;

export class TableFacade {
  private readonly name: string;
  private readonly columns: ColumnBuilder[] = [];
  private readonly foreignKeys: ForeignKeyBuilder[] = [];
  private readonly indices: IndexBuilder[] = [];
  private readonly uniques: UniqueBuilder[] = [];

  constructor(name: string) {
    this.name = name;
  }

  column(column: TypeormTableColumnOptions): TableFacade {
    this.columns.push(ColumnBuilder.from(column));
    return this;
  }

  foreignKey(fk: TypeormTableForeignKeyOptions): TableFacade;
  foreignKey(
    column: string,
    foreignTable: string,
    foreignColumn: string,
    fkName?: string
  ): ForeignKeyBuilder;
  foreignKey(
    optionsOrColumn: TypeormTableForeignKeyOptions | string,
    foreignTable?: string,
    foreignColumn?: string,
    name?: string
  ): any {
    if (typeof optionsOrColumn === 'string') {
      name = name ? name : this.generateName('fk');
      const localColumn = optionsOrColumn as string;

      const foreignKeyBuilder = new ForeignKeyBuilder(
        name,
        [localColumn],
        foreignTable,
        [foreignColumn]
      );
      this.foreignKeys.push(foreignKeyBuilder);
      return foreignKeyBuilder;
    }

    const options = {
      ...optionsOrColumn,
      name: optionsOrColumn.name
        ? optionsOrColumn.name
        : this.generateName('fk'),
    };

    this.foreignKeys.push(ForeignKeyBuilder.from(options));
    return this;
  }

  index(index: TypeormTableIndexOptions): TableFacade;
  index(columns: string[], indexName?: string): IndexBuilder;
  index(
    optionsOrColumns: TypeormTableIndexOptions | string[],
    indexName?: string
  ): any {
    if (Array.isArray(optionsOrColumns)) {
      const name = indexName ? indexName : this.generateName('index');
      const columns = optionsOrColumns as string[];

      const indexBuilder = new IndexBuilder(name, columns);
      this.indices.push(indexBuilder);
      return indexBuilder;
    }

    const options = {
      ...optionsOrColumns,
      name: optionsOrColumns.name
        ? optionsOrColumns.name
        : this.generateName('index'),
    };

    this.indices.push(IndexBuilder.from(options));
    return this;
  }

  unique(unique: TypeormTableUniqueOptions): TableFacade;
  unique(columns: string[], uniqueName?: string): UniqueBuilder;
  unique(
    optionsOrColumns: TypeormTableUniqueOptions | string[],
    uniqueName?: string
  ): any {
    if (Array.isArray(optionsOrColumns)) {
      const name = uniqueName ? uniqueName : this.generateName('unique');
      const columns = optionsOrColumns as string[];

      const uniqueBuilder = new UniqueBuilder(name, columns);
      this.uniques.push(uniqueBuilder);
      return uniqueBuilder;
    }

    const options = {
      ...optionsOrColumns,
      name: optionsOrColumns.name
        ? optionsOrColumns.name
        : this.generateName('unique'),
    };

    this.uniques.push(UniqueBuilder.from(options));
    return this;
  }

  create(builder: BuilderFn): TypeormTable {
    builder(
      new Proxy<TableFacade>(this, {
        get(target: TableFacade, name: string): any {
          if (target[name] !== undefined) return target[name];

          if (typeof Columns[name] === 'function') {
            return function (...args) {
              const column: ColumnBuilder | ColumnBuilder[] = Columns[name](
                ...args
              );

              if (Array.isArray(column)) {
                target.columns.push(...column);
              } else {
                target.columns.push(column);
              }

              return column;
            };
          }

          throw new Error('no such property - ' + name);
        },
      })
    );

    const tableBuilder = new TableBuilder(this.name);

    this.columns.forEach((column) => tableBuilder.column(column.build()));
    this.foreignKeys.forEach((fk) => tableBuilder.foreignKey(fk.build()));
    this.indices.forEach((index) => tableBuilder.index(index.build()));
    this.uniques.forEach((unique) => tableBuilder.unique(unique.build()));

    return tableBuilder.build();
  }

  // ----- override some of Columns functions -------------------------

  manageable({ setFK = true }: { setFK?: boolean } = {}): ColumnBuilder[] {
    const columns = Columns.manageable();
    this.columns.push(...columns);

    if (setFK) {
      this.foreignKey('deleted_by', 'users', 'id');
      this.foreignKey('created_by', 'users', 'id');
      this.foreignKey('updated_by', 'users', 'id');
    }

    return columns;
  }

  refId(foreignTable: string, name?: string): ColumnBuilder {
    const column = Columns.refId(foreignTable, name);
    this.columns.push(column);

    this.foreignKey(column.options.name, foreignTable, 'id');

    return column;
  }

  // ----- private methods --------------------

  private generateName(type: 'fk' | 'index' | 'unique'): string {
    const nextNumber = (type: 'fk' | 'index' | 'unique'): number => {
      switch (type) {
        case 'fk':
          return this.foreignKeys.length + 1;
        case 'index':
          return this.indices.length + 1;
        case 'unique':
          return this.uniques.length + 1;
      }
    };

    return `${this.name}_${type}_${nextNumber(type)}`;
  }
}
