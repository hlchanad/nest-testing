import { TypeormTableColumn } from '../../../migration.interfaces';
import { ColumnBuilder } from '../../builders';
import * as ColumnTypes from '../index';

type TypeormTableColumnField =
  | 'name'
  | 'type'
  | 'default'
  | 'onUpdate'
  | 'isNullable'
  | 'isGenerated'
  | 'generationStrategy'
  | 'isPrimary'
  | 'isUnique'
  | 'isArray'
  | 'comment'
  | 'length'
  | 'width'
  | 'charset'
  | 'collation'
  | 'precision'
  | 'scale'
  | 'zerofill'
  | 'unsigned'
  | 'enum'
  | 'enumName'
  | 'asExpression'
  | 'generatedType'
  | 'spatialFeatureType'
  | 'srid';

function check(
  builder: ColumnBuilder,
  result: TypeormTableColumn,
  extraFields: TypeormTableColumnField[] = []
) {
  const fields: TypeormTableColumnField[] = ['name', 'type', ...extraFields];
  fields.forEach((field) => expect(result[field]).toBe(builder.options[field]));
}

describe('ColumnTypes', () => {
  describe('Normal Data Types', () => {
    it('returns a ColumnBuilder of varchar', () => {
      const builder = ColumnTypes.varchar('column', 1023);
      const result = builder.build();

      check(builder, result, ['length']);
    });

    it('returns a ColumnBuilder of text', () => {
      const builder = ColumnTypes.text('column');
      const result = builder.build();

      check(builder, result);
    });

    it('returns a ColumnBuilder of int', () => {
      const builder = ColumnTypes.int('column');
      const result = builder.build();

      check(builder, result);
    });

    it('returns a ColumnBuilder of bigInt', () => {
      const builder = ColumnTypes.bigInt('column');
      const result = builder.build();

      check(builder, result);
    });

    it('returns a ColumnBuilder of decimal', () => {
      const builder = ColumnTypes.decimal('column', 10, 2);
      const result = builder.build();

      check(builder, result, ['length']);
    });

    it('returns a ColumnBuilder of bit', () => {
      const builder = ColumnTypes.bit('column', 123);
      const result = builder.build();

      check(builder, result, ['length']);
    });

    it('returns a ColumnBuilder of date', () => {
      const builder = ColumnTypes.date('column');
      const result = builder.build();

      check(builder, result);
    });

    it('returns a ColumnBuilder of datetime', () => {
      const builder = ColumnTypes.datetime('column');
      const result = builder.build();

      check(builder, result);
    });

    it('returns a ColumnBuilder of enums', () => {
      const builder = ColumnTypes.enums('column', ['A', 'B', 'C']);
      const result = builder.build();

      check(builder, result, ['enum']);
    });
  });

  describe('Custom Data Types', () => {
    it('returns a ColumnBuilder of id', () => {
      const builder = ColumnTypes.id();
      const result = builder.build();

      check(builder, result, ['isPrimary', 'isNullable']);
      expect(result.name).toBe('id');
    });

    it('returns a ColumnBuilder of increments', () => {
      const builder = ColumnTypes.increments('column');
      const result = builder.build();

      check(builder, result, ['unsigned', 'isGenerated', 'generationStrategy']);
    });

    it('returns a ColumnBuilder of bigIncrements', () => {
      const builder = ColumnTypes.bigIncrements('column');
      const result = builder.build();

      check(builder, result, ['unsigned', 'isGenerated', 'generationStrategy']);
    });

    it('returns a ColumnBuilder of boolean', () => {
      const builder = ColumnTypes.boolean('column');
      const result = builder.build();

      check(builder, result, ['length']);
    });

    it('returns a ColumnBuilder of manageable', () => {
      const builders = ColumnTypes.manageable();
      const results = builders.map((builder) => builder.build());

      builders.forEach((builder, index) => check(builder, results[index]));

      check(builders[0], results[0], ['length']); // status: varchar
      check(builders[3], results[3], ['default']); // created_at: datetime
      check(builders[5], results[5], ['default']); // updated_at: datetime
      check(builders[7], results[7], ['default', 'isNullable']); // version: int
    });

    it('returns a ColumnBuilder of refId', () => {
      const builder = ColumnTypes.refId('articles');
      const result = builder.build();

      check(builder, result, ['unsigned']);
    });

    it('returns a ColumnBuilder of language', () => {
      const builder = ColumnTypes.language();
      const result = builder.build();

      check(builder, result, []);
    });
  });
});
