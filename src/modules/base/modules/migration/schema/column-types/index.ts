import * as pluralize from 'pluralize';

import { ColumnBuilder } from '../builders';

/***** IMPORTANT: remember to update table-facade.declaration.ts when updating this file *****/

/* Normal Data Types */

export function varchar(name: string, length: number = 1023): ColumnBuilder {
  return new ColumnBuilder(name, 'varchar').length(length.toString());
}

export function text(name: string): ColumnBuilder {
  return new ColumnBuilder(name, 'text');
}

export function int(name: string): ColumnBuilder {
  return new ColumnBuilder(name, 'int');
}

export function bigInt(name: string): ColumnBuilder {
  return new ColumnBuilder(name, 'bigInt');
}

export function decimal(
  name: string,
  total: number,
  decimal: number
): ColumnBuilder {
  return new ColumnBuilder(name, 'decimal').length(`${total},${decimal}`);
}

export function bit(name: string, length: number): ColumnBuilder {
  return new ColumnBuilder(name, 'bit').length(length.toString());
}

export function date(name: string): ColumnBuilder {
  return new ColumnBuilder(name, 'date');
}

export function datetime(name: string): ColumnBuilder {
  return new ColumnBuilder(name, 'datetime');
}

export function enums(
  name: string,
  enums: string[],
  enumName?: string
): ColumnBuilder {
  return new ColumnBuilder(name, 'enum').enum(enums, enumName);
}

/* Custom Data Types */

export function id(): ColumnBuilder {
  return bigIncrements('id').primary().notNull();
}

export function increments(name: string): ColumnBuilder {
  return int(name).unsigned().autoIncrement();
}

export function bigIncrements(name: string): ColumnBuilder {
  return bigInt(name).unsigned().autoIncrement();
}

export function boolean(name: string): ColumnBuilder {
  return bit(name, 1);
}

export function manageable(): ColumnBuilder[] {
  return [
    varchar('status'),
    datetime('deleted_at'),
    bigInt('deleted_by').unsigned(),
    datetime('created_at').default('CURRENT_TIMESTAMP'),
    bigInt('created_by').unsigned(),
    datetime('updated_at').default('CURRENT_TIMESTAMP'),
    bigInt('updated_by').unsigned(),
    int('version').default((1).toString()).notNull(),
  ];
}

export function refId(foreignTable: string, name?: string): ColumnBuilder {
  const columnName = name || `${pluralize.singular(foreignTable)}_id`;
  return bigInt(columnName).unsigned();
}

export function language(): ColumnBuilder {
  return varchar('language').notNull();
}
