import {
  TypeormTableForeignKey,
  TypeormTableForeignKeyOptions,
} from '../../migration.interfaces';

export class ForeignKeyBuilder {
  options: TypeormTableForeignKeyOptions;

  static from(options: TypeormTableForeignKeyOptions): ForeignKeyBuilder {
    const foreignKeyBuilder = new ForeignKeyBuilder(
      options.name,
      options.columnNames,
      options.referencedTableName,
      options.referencedColumnNames
    );
    foreignKeyBuilder.options = options;
    return foreignKeyBuilder;
  }

  constructor(
    name: string,
    localColumns: string[],
    foreignTable: string,
    foreignColumns: string[]
  ) {
    this.options = {
      name,
      columnNames: localColumns,
      referencedTableName: foreignTable,
      referencedColumnNames: foreignColumns,
    };
  }

  onDelete(onDelete: string): ForeignKeyBuilder {
    this.options = { ...this.options, onDelete };
    return this;
  }

  onUpdate(onUpdate: string): ForeignKeyBuilder {
    this.options = { ...this.options, onUpdate };
    return this;
  }

  build(): TypeormTableForeignKey {
    return new TypeormTableForeignKey(this.options);
  }
}
