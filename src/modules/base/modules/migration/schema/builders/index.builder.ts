import {
  TypeormTableIndex,
  TypeormTableIndexOptions,
} from '../../migration.interfaces';

export class IndexBuilder {
  options: TypeormTableIndexOptions;

  static from(options: TypeormTableIndexOptions): IndexBuilder {
    const indexBuilder = new IndexBuilder(options.name, options.columnNames);
    indexBuilder.options = options;
    return indexBuilder;
  }

  constructor(name: string, columnNames: string[]) {
    this.options = { name, columnNames };
  }

  unique(): IndexBuilder {
    this.options = { ...this.options, isUnique: true };
    return this;
  }

  build(): TypeormTableIndex {
    return new TypeormTableIndex(this.options);
  }
}
