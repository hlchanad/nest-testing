import {
  TypeormTableUnique,
  TypeormTableUniqueOptions,
} from '../../migration.interfaces';

export class UniqueBuilder {
  options: TypeormTableUniqueOptions;

  static from(options: TypeormTableUniqueOptions): UniqueBuilder {
    const uniqueBuilder = new UniqueBuilder(options.name, options.columnNames);
    uniqueBuilder.options = options;
    return uniqueBuilder;
  }

  constructor(name: string, columnNames: string[]) {
    this.options = { name, columnNames };
  }

  build(): TypeormTableUnique {
    return new TypeormTableUnique(this.options);
  }
}
