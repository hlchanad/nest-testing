import {
  TypeormTable,
  TypeormTableColumn,
  TypeormTableColumnOptions,
  TypeormTableForeignKey,
  TypeormTableForeignKeyOptions,
  TypeormTableIndex,
  TypeormTableIndexOptions,
  TypeormTableUnique,
  TypeormTableUniqueOptions,
} from '../../migration.interfaces';

export class TableBuilder {
  public readonly name: string;
  private readonly columns: TypeormTableColumn[] = [];
  private readonly indices: TypeormTableIndex[] = [];
  private readonly foreignKeys: TypeormTableForeignKey[] = [];
  private readonly uniques: TypeormTableUnique[] = [];

  constructor(name: string) {
    this.name = name;
  }

  column(column: TypeormTableColumn | TypeormTableColumnOptions): TableBuilder {
    this.columns.push(
      column instanceof TypeormTableColumn
        ? column
        : new TypeormTableColumn(column)
    );
    return this;
  }

  index(index: TypeormTableIndex | TypeormTableIndexOptions): TableBuilder {
    this.indices.push(
      index instanceof TypeormTableIndex ? index : new TypeormTableIndex(index)
    );
    return this;
  }

  foreignKey(
    fk: TypeormTableForeignKey | TypeormTableForeignKeyOptions
  ): TableBuilder {
    this.foreignKeys.push(
      fk instanceof TypeormTableForeignKey ? fk : new TypeormTableForeignKey(fk)
    );
    return this;
  }

  unique(unique: TypeormTableUnique | TypeormTableUniqueOptions): TableBuilder {
    this.uniques.push(
      unique instanceof TypeormTableUnique
        ? unique
        : new TypeormTableUnique(unique)
    );
    return this;
  }

  build(): TypeormTable {
    return new TypeormTable({
      name: this.name,
      columns: this.columns,
      indices: this.indices,
      foreignKeys: this.foreignKeys,
      uniques: this.uniques,
    });
  }
}
