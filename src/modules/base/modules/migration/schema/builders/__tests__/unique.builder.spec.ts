import { UniqueBuilder } from '../unique.builder';
import { TypeormTableUnique } from '../../../migration.interfaces';

function getBuilder(): UniqueBuilder {
  return new UniqueBuilder('index', ['column']);
}

function baseExpect(builder: UniqueBuilder, result: TypeormTableUnique) {
  expect(result).toBeInstanceOf(TypeormTableUnique);
  expect(result.name).toBe(builder.options.name);
  expect(result.columnNames).toBe(builder.options.columnNames);
}

describe('UniqueBuilder', () => {
  it('builds TypeormTableUnique object', () => {
    const builder = getBuilder();
    const result = builder.build();

    baseExpect(builder, result);
  });

  describe('Mutations', () => {});
});
