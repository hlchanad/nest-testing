import {
  TypeormTable,
  TypeormTableColumn,
  TypeormTableForeignKey,
  TypeormTableIndex,
  TypeormTableUnique,
} from '../../../migration.interfaces';
import { TableBuilder } from '../table.builder';

function getBuilder(): TableBuilder {
  return new TableBuilder('table');
}

function baseExpect(builder: TableBuilder, result: TypeormTable) {
  // getting private properties inside `builder`
  const columns = (builder['columns'] as unknown) as TypeormTableColumn[];
  const fks = (builder['foreignKeys'] as unknown) as TypeormTableForeignKey[];
  const indices = (builder['indices'] as unknown) as TypeormTableIndex[];
  const uniques = (builder['uniques'] as unknown) as TypeormTableUnique[];

  expect(result).toBeInstanceOf(TypeormTable);
  expect(result.columns).toStrictEqual(columns);
  expect(result.foreignKeys).toStrictEqual(fks);
  expect(result.indices).toStrictEqual(indices);
  expect(result.uniques).toStrictEqual(uniques);
}

describe('TableBuilder', () => {
  it('builds TypeormTable object', () => {
    const builder = getBuilder();
    const result = builder.build();

    baseExpect(builder, result);
  });

  it('pushes column', () => {
    const builder = getBuilder().column({ name: 'column', type: 'varchar' });
    const result = builder.build();

    baseExpect(builder, result);
  });

  it('pushes foreignKey', () => {
    const builder = getBuilder().foreignKey({
      name: 'fk',
      columnNames: ['created_by'],
      referencedTableName: 'users',
      referencedColumnNames: ['id'],
    });
    const result = builder.build();

    baseExpect(builder, result);
  });

  it('pushes index', () => {
    const builder = getBuilder().index({ name: 'index', columnNames: ['id'] });
    const result = builder.build();

    baseExpect(builder, result);
  });

  it('pushes unique', () => {
    const builder = getBuilder().unique({
      name: 'unique',
      columnNames: ['id'],
    });
    const result = builder.build();

    baseExpect(builder, result);
  });
});
