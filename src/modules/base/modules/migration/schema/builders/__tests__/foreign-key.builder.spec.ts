import { ForeignKeyBuilder } from '../foreign-key.builder';
import { TypeormTableForeignKey } from '../../../migration.interfaces';

function getBuilder(): ForeignKeyBuilder {
  return new ForeignKeyBuilder('test', ['created_by'], 'users', ['id']);
}

function baseExpect(
  builder: ForeignKeyBuilder,
  result: TypeormTableForeignKey
) {
  expect(result).toBeInstanceOf(TypeormTableForeignKey);
  expect(result.name).toBe(builder.options.name);
  expect(result.columnNames).toBe(builder.options.columnNames);
  expect(result.referencedTableName).toBe(builder.options.referencedTableName);
  expect(result.referencedColumnNames).toBe(
    builder.options.referencedColumnNames
  );
}

describe('ForeignKeyBuilder', () => {
  it('builds TypeormTableForeignKey object', () => {
    const builder = getBuilder();
    const result = builder.build();

    baseExpect(builder, result);
  });

  describe('Mutations', () => {
    it('sets onDelete', () => {
      const builder = getBuilder().onDelete('ON DELETE');
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.onDelete).toBe(builder.options.onDelete);
    });

    it('sets onUpdate', () => {
      const builder = getBuilder().onUpdate('ON UPDATE');
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.onUpdate).toBe(builder.options.onUpdate);
    });
  });
});
