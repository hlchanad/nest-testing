import { ColumnBuilder } from '../column.builder';
import { TypeormTableColumn } from '../../../migration.interfaces';

function getBuilder(): ColumnBuilder {
  return new ColumnBuilder('test', 'varchar');
}

function baseExpect(builder: ColumnBuilder, result: TypeormTableColumn) {
  expect(result).toBeInstanceOf(TypeormTableColumn);
  expect(result.name).toBe(builder.options.name);
  expect(result.type).toBe(builder.options.type);
}

describe('ColumnBuilder', () => {
  it('builds TypeormTableColumn object', () => {
    const builder = getBuilder();
    const result = builder.build();

    baseExpect(builder, result);
  });

  describe('Mutations', () => {
    it('sets length', () => {
      const builder = getBuilder().length((1234).toString());
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.length).toBe(builder.options.length);
    });

    it('sets isNullable', () => {
      const builder = getBuilder().notNull();
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.isNullable).toBeFalsy();
    });

    it('sets isPrimary', () => {
      const builder = getBuilder().primary();
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.isPrimary).toBeTruthy();
    });

    it('sets isUnique', () => {
      const builder = getBuilder().unique();
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.isUnique).toBeTruthy();
    });

    it('sets unsigned', () => {
      const builder = getBuilder().unsigned();
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.unsigned).toBeTruthy();
    });

    it('sets "Auto Increment"', () => {
      const builder = getBuilder().autoIncrement();
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.isGenerated).toBeTruthy();
      expect(result.generationStrategy).toBe('increment');
    });

    it('sets enum', () => {
      const builder = getBuilder().enum(['A', 'B', 'C'], 'abc');
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.enumName).toBe(builder.options.enumName);
      expect(result.enum).toBe(builder.options.enum);
    });

    it('sets default', () => {
      const builder = getBuilder().default('hello world');
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.default).toBe(builder.options.default);
    });

    it('sets comment', () => {
      const builder = getBuilder().comment(
        'this is the comment for the column'
      );
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.comment).toBe(builder.options.comment);
    });
  });
});
