import { IndexBuilder } from '../index.builder';
import { TypeormTableIndex } from '../../../migration.interfaces';

function getBuilder(): IndexBuilder {
  return new IndexBuilder('index', ['column']);
}

function baseExpect(builder: IndexBuilder, result: TypeormTableIndex) {
  expect(result).toBeInstanceOf(TypeormTableIndex);
  expect(result.name).toBe(builder.options.name);
  expect(result.columnNames).toBe(builder.options.columnNames);
}

describe('IndexBuilder', () => {
  it('builds TypeormTableIndex object', () => {
    const builder = getBuilder();
    const result = builder.build();

    baseExpect(builder, result);
  });

  describe('Mutations', () => {
    it('sets unique', () => {
      const builder = getBuilder().unique();
      const result = builder.build();

      baseExpect(builder, result);
      expect(result.isUnique).toBeTruthy();
    });
  });
});
