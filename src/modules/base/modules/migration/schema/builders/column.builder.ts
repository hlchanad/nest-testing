import {
  TypeormTableColumn,
  TypeormTableColumnOptions,
} from '../../migration.interfaces';

export class ColumnBuilder {
  options: TypeormTableColumnOptions;

  static from(options: TypeormTableColumnOptions): ColumnBuilder {
    const columnBuilder = new ColumnBuilder(options.name, options.type);
    columnBuilder.options = options;
    return columnBuilder;
  }

  constructor(name: string, type: string) {
    this.options = { name, type, isNullable: true };
  }

  length(length: string): ColumnBuilder {
    this.options = { ...this.options, length: length };
    return this;
  }

  notNull(): ColumnBuilder {
    this.options = { ...this.options, isNullable: false };
    return this;
  }

  primary(): ColumnBuilder {
    this.options = { ...this.options, isPrimary: true };
    return this;
  }

  unique(): ColumnBuilder {
    this.options = { ...this.options, isUnique: true };
    return this;
  }

  unsigned(): ColumnBuilder {
    this.options = { ...this.options, unsigned: true };
    return this;
  }

  autoIncrement(): ColumnBuilder {
    this.options = {
      ...this.options,
      isGenerated: true,
      generationStrategy: 'increment',
    };
    return this;
  }

  enum(enums: string[], name?: string): ColumnBuilder {
    this.options = { ...this.options, enum: enums, enumName: name };
    return this;
  }

  default(value: string): ColumnBuilder {
    this.options = { ...this.options, default: value };
    return this;
  }

  comment(comment: string): ColumnBuilder {
    this.options = { ...this.options, comment };
    return this;
  }

  build(): TypeormTableColumn {
    return new TypeormTableColumn(this.options);
  }
}
