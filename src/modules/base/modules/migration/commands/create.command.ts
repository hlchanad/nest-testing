import * as _ from 'lodash';
import * as fs from 'fs';
import * as chalk from 'chalk';
import * as capitalize from 'capitalize';
import { Arguments } from 'yargs';

import { Command } from '../../command';
import { TemplateService } from '../../template';

import { MigrationService } from '../migration.service';

const getTemplate = (name: string, timestamp: string) => {
  let tableName = name;

  const isCreate = _.kebabCase(name).split('-').includes('create');
  if (isCreate) {
    tableName = _.kebabCase(name)
      .split('-')
      .filter((part) => part !== 'create')
      .join('-');
  }

  return TemplateService.render({
    path: __dirname + '/../views/migration.hbs',
    data: {
      tableName,
      isCreate,
      className: capitalize(_.camelCase(name), true) + timestamp,
    },
  });
};

// copy logic from 'typeorm/commands/MigrationCreateCommand'
export function CreateCommand(migrationService: MigrationService): Command {
  return {
    command: 'migration:create [name]',
    describe: 'Create migration',
    builder: (yargs) => yargs.demandOption('name'),
    handler: async (args: Arguments<{ name: string }>) => {
      const timestamp = new Date().getTime().toString();
      const filename = timestamp + '-' + args.name + '.ts';
      const directory = migrationService.migrationDir();

      const fileContent = getTemplate(args.name, timestamp);
      const path =
        process.cwd() + '/' + (directory ? directory + '/' : '') + filename;

      fs.writeFileSync(path, fileContent);

      console.log(`Created migration file ${chalk.blue(path)}`);
    },
  };
}
