import { Command } from '../../command';

import { MigrationService } from '../migration.service';

export function RevertCommand(migrationService: MigrationService): Command {
  return {
    command: 'migration:revert',
    describe: 'Revert last migration',
    handler: async (args) => {
      await migrationService.revert();
      console.log('Reverted last migration');
    },
  };
}
