import { Command } from '../../command';

import { MigrationService } from '../migration.service';

export function RunCommand(migrationService: MigrationService): Command {
  return {
    command: 'migration:run',
    describe: 'Run migration',
    handler: async (args) => {
      await migrationService.run();
      console.log('Ran migrations');
    },
  };
}
