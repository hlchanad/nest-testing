import { ModuleMetadata } from '@nestjs/common/interfaces';
import { ConnectionOptions } from 'typeorm';

export type MigrationModuleOptions = ConnectionOptions;

export interface MigrationModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (
    ...args: any[]
  ) => Promise<MigrationModuleOptions> | MigrationModuleOptions;
  inject: any[];
}

// re-export from typeorm
export {
  Table as TypeormTable,
  TableColumn as TypeormTableColumn,
  TableForeignKey as TypeormTableForeignKey,
  TableIndex as TypeormTableIndex,
  TableUnique as TypeormTableUnique,
} from 'typeorm';
export { TableColumnOptions as TypeormTableColumnOptions } from 'typeorm/schema-builder/options/TableColumnOptions';
export { TableForeignKeyOptions as TypeormTableForeignKeyOptions } from 'typeorm/schema-builder/options/TableForeignKeyOptions';
export { TableIndexOptions as TypeormTableIndexOptions } from 'typeorm/schema-builder/options/TableIndexOptions';
export { TableUniqueOptions as TypeormTableUniqueOptions } from 'typeorm/schema-builder/options/TableUniqueOptions';
