import { DynamicModule, Module } from '@nestjs/common';

import { CommandService } from '../command';

import {
  MigrationModuleAsyncOptions,
  MigrationModuleOptions,
} from './migration.interfaces';
import { MIGRATION_MODULE_OPTIONS } from './migration.constants';
import { MigrationService } from './migration.service';
import { CreateCommand, RevertCommand, RunCommand } from './commands';

@Module({})
export class MigrationModule {
  constructor(
    readonly commandService: CommandService,
    readonly migrationService: MigrationService
  ) {
    commandService.register(CreateCommand(migrationService));
    commandService.register(RunCommand(migrationService));
    commandService.register(RevertCommand(migrationService));
  }

  static register(options: MigrationModuleOptions): DynamicModule {
    return {
      module: MigrationModule,
      providers: [
        {
          provide: MIGRATION_MODULE_OPTIONS,
          useValue: options,
        },
        MigrationService,
      ],
    };
  }

  static registerAsync(options: MigrationModuleAsyncOptions): DynamicModule {
    return {
      module: MigrationModule,
      imports: options.imports,
      providers: [
        {
          provide: MIGRATION_MODULE_OPTIONS,
          useFactory: options.useFactory,
          inject: options.inject,
        },
        MigrationService,
      ],
    };
  }
}
