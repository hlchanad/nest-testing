import { Inject, Injectable } from '@nestjs/common';
import { Connection, createConnection } from 'typeorm';

import { sleep } from '@helpers';

import { MIGRATION_MODULE_OPTIONS } from './migration.constants';
import { MigrationModuleOptions } from './migration.interfaces';

@Injectable()
export class MigrationService {
  private connection: Connection;

  constructor(
    @Inject(MIGRATION_MODULE_OPTIONS)
    private readonly options: MigrationModuleOptions
  ) {
    createConnection(options).then(
      (connection) => (this.connection = connection)
    );
  }

  async run(options?: { transaction?: 'all' | 'none' | 'each' }) {
    await this.waitConnection();
    await this.connection.runMigrations(options);
  }

  async revert(options?: { transaction?: 'all' | 'none' | 'each' }) {
    await this.waitConnection();
    await this.connection.undoLastMigration(options);
  }

  migrationDir(): string {
    return this.options.cli ? this.options.cli.migrationsDir : undefined;
  }

  // TODO: maybe use yield to do??
  private async waitConnection() {
    while (this.connection == undefined) {
      await sleep(100);
    }
  }
}
