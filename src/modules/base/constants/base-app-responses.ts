import { AppResponse } from '../models';

// 2XX
export const OK = new AppResponse('200.0000', 'OK');

// 4XX
export const InvalidInput = new AppResponse('400.0000', 'Invalid input');
export const RecordNotFound = new AppResponse('404.0000', 'Record not found');

// 5XX
export const SomethingWentWrong = new AppResponse(
  '500.0000',
  'Something went wrong'
);
