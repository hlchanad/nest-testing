import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CommandService } from '@base';

import { GetByIdCommand } from './commands';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {
  constructor(
    private readonly commandService: CommandService,
    private readonly usersService: UsersService
  ) {
    this.commandService.register(GetByIdCommand(usersService));
  }
}
