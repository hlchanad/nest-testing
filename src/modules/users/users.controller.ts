import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';

import {
  AppResponse,
  BaseAppResponses,
  BaseController,
  Logger,
  RecordNotFoundException,
} from '@base';

import { UsersService } from './users.service';
import { User } from './user.entity';
import { CreateUserDto, UpdateUserDto } from './dtos';

@Controller('/users')
export class UsersController extends BaseController {
  constructor(private usersService: UsersService, private logger: Logger) {
    super();
    this.logger.setContext(UsersController.name);
  }

  @Get()
  async findAll(): Promise<AppResponse<User[]>> {
    this.logger.info('/users');
    const users = await this.usersService.findAll();
    return BaseAppResponses.OK.clone<User[]>().setData(users);
  }

  @Get('/:id')
  async findOne(@Param('id') id: number): Promise<AppResponse<User>> {
    const user = await this.usersService.findOne(id);

    if (!user) {
      throw new RecordNotFoundException();
    }

    return BaseAppResponses.OK.clone<User>().setData(user);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() createUserDto: CreateUserDto
  ): Promise<AppResponse<User>> {
    const user = await this.usersService.create(createUserDto);
    return BaseAppResponses.OK.clone<User>().setData(user);
  }

  @Put('/:id')
  async update(
    @Param('id') id: number,
    @Body() updateUserDto: UpdateUserDto
  ): Promise<AppResponse<any>> {
    const user = await this.usersService.findOne(id);

    if (!user) {
      throw new RecordNotFoundException();
    }

    await this.usersService.update(id, updateUserDto);

    return BaseAppResponses.OK.clone();
  }

  @Delete('/:id')
  async remove(@Param('id') id: number): Promise<AppResponse<any>> {
    const user = await this.usersService.findOne(id);

    if (!user) {
      throw new RecordNotFoundException();
    }

    await this.usersService.remove(id);

    return BaseAppResponses.OK.clone();
  }
}
