import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BaseService } from '@base';

import { User } from './user.entity';
import { CreateUserDto, UpdateUserDto } from './dtos';

@Injectable()
export class UsersService extends BaseService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) {
    super();
  }

  async findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async findOne(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = this.usersRepository.create(createUserDto);
    return this.usersRepository.save(user);
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<boolean> {
    const updateResult = await this.usersRepository.update(id, updateUserDto);
    return updateResult.affected > 0;
  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
