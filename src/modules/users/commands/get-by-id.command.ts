import { Arguments } from 'yargs';
import * as chalk from 'chalk';

import { Command } from '@base';

import { UsersService } from '../users.service';

export function GetByIdCommand(usersService: UsersService): Command {
  return {
    command: 'users:id [id]',
    describe: 'Get user by ID',
    builder: (yargs) => yargs.demandOption('id'),
    handler: async (args: Arguments<{ id: number }>) => {
      const user = await usersService.findOne(args.id);

      if (!user) {
        console.error(chalk.red('User not found'));
        return;
      }

      console.log(user);
    },
  };
}
