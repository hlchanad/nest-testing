import { IsBoolean, IsEmail } from 'class-validator';

export class UpdateUserDto {
  username: string;

  password: string;

  @IsEmail()
  email: string;

  firstName: string;

  lastName: string;

  @IsBoolean()
  isActive: boolean;
}
