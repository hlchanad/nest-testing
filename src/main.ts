import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';

import { LOGGER_MODULE_NEST_LOGGER } from '@base';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, // remove un-listed properties from DTOs
      transform: true, // transform to DTO type
    })
  );
  app.useLogger(app.get(LOGGER_MODULE_NEST_LOGGER));
  await app.listen(app.get<ConfigService>('ConfigService').get('app.port'));
}

bootstrap();
