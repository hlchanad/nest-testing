import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BaseModule } from '@base';

import { config } from '@helpers';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { UsersModule } from './modules/users/users.module';
import { ArticlesModule } from './modules/articles';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [...config.factories()],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) =>
        configService.get('database.primary'),
      inject: [ConfigService],
    }),
    BaseModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        ...configService.get('base'),
        loggerModuleOptions: configService.get('logger'),
        migrationModuleOptions: configService.get('database.primary'),
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    ArticlesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
