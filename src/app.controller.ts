import { Controller, Get, HttpStatus } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { AppException, BaseAppResponses, Logger } from '@base';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly configService: ConfigService,
    private readonly logger: Logger
  ) {
    this.logger.setContext(AppController.name);
  }

  @Get()
  getHello(): string {
    this.logger.info('Hello World!', { hello: 'world' });
    this.logger.debug('debug level');
    this.logger.warn('warn level');
    this.logger.verbose('verbose level');
    this.logger.error('error level');

    return (
      this.appService.getHello() +
      ' by ' +
      this.configService.get<String>('app.name')
    );
  }

  @Get('/error')
  getError(): string {
    const a = null;
    a.b = 1;
    return '';
  }

  @Get('/app-error')
  getAppError(): string {
    throw new AppException(
      HttpStatus.INTERNAL_SERVER_ERROR,
      BaseAppResponses.SomethingWentWrong.clone().setDetail('You naughty boi')
    );
  }

  @Get('/async-error')
  getAsyncError(): string {
    const a = null;
    a.b = 1;
    return '';
  }

  @Get('/async-app-error')
  getAsyncAppError(): string {
    throw new AppException(
      HttpStatus.INTERNAL_SERVER_ERROR,
      BaseAppResponses.SomethingWentWrong.clone().setDetail('You naughty boi')
    );
  }
}
