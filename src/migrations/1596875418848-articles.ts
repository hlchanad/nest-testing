import { MigrationInterface, QueryRunner } from 'typeorm';

import { TableFacade } from '@base';

export class Articles1596875418848 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new TableFacade('articles').create((table) => {
        table.id();
        table.datetime('publish_at').notNull();
        table.manageable();
      })
    );

    await queryRunner.createTable(
      new TableFacade('article_translations').create((table) => {
        table.id();
        table.refId('articles').notNull();
        table.language().notNull();
        table.text('title').notNull();
        table.text('content').notNull();
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('article_translations');
    await queryRunner.dropTable('articles');
  }
}
