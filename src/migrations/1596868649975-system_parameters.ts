import { MigrationInterface, QueryRunner } from 'typeorm';

import { TableFacade, Columns } from '@base';

export class SystemParameters1596868649975 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new TableFacade('system_parameters').create((table) => {
        table.id();
        table.varchar('key').notNull();
        table.varchar('value', 4095).notNull();
        // table.text('description');
        table.manageable();
      })
    );

    await queryRunner.addColumns('system_parameters', [
      Columns.text('description').build(),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('system_parameters');
  }
}
