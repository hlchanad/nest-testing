import { MigrationInterface, QueryRunner } from 'typeorm';

import { TableFacade } from '@base';

export class Users1596867855216 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new TableFacade('users').create((table) => {
        table.id();
        table.varchar('username').notNull();
        table.varchar('password').notNull();
        table.varchar('email').notNull();
        table.varchar('first_name').notNull();
        table.varchar('last_name');
        table.boolean('is_active').default('true').notNull();
        table.manageable();
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('users');
  }
}
