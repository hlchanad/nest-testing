import * as yargs from 'yargs';
import { NestFactory } from '@nestjs/core';

import { CommandService } from '@base';

import { AppModule } from '@src/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const commandService: CommandService = app.get(CommandService);

  commandService.list().forEach((command) => yargs.command(command));
  yargs.strict().onFinishCommand((result) => process.exit(0)).argv;
}

bootstrap();
