import { Test, TestingModule } from '@nestjs/testing';
import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';

import { AppController } from '../app.controller';
import { AppService } from '../app.service';
import { BaseModule } from '@base';

@Injectable()
class ConfigService {
  config = {
    app: { name: 'jest' },
  };

  get(key: string) {
    return _.get(this.config, key);
  }
}

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [
        BaseModule.register({ migrationModuleOptions: { type: 'mysql' } }),
      ],
      controllers: [AppController],
      providers: [AppService, ConfigService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe(
        `Hello World! by ${new ConfigService().get('app.name')}`
      );
    });
  });
});
