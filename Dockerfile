# builder
FROM node:14.3.0-alpine3.11 as builder

WORKDIR /usr/app

COPY ./package*.json ./
RUN npm install

COPY ./ ./
RUN npm run build

# image base
FROM node:14.3.0-alpine3.11 as base

WORKDIR /usr/app

COPY --from=builder /usr/app/package*.json ./
RUN npm install --production

COPY --from=builder /usr/app/dist ./dist

# app
FROM base as app

WORKDIR /usr/app

CMD npm run start
